
class Hogar{
    nombre;
    ID;
    Edad;
    Peso;
    Estatura;
    Personaldad;
    Profesion;
    Hobby;
    Amigos;

    laborEnCasa(){
      Aseo();
      Cocinar(); 
    }
    GestionarTiempoLibre(){
        console.log("Sabados, Domingos y Festivos");
    }
}

function Aseo(){
    console.log("Lavar Ropa"); 
    console.log("Lavar Loza"); 
    console.log("Barrer"); 
    console.log("Trapear"); 
}

function Cocinar(){
    console.log("Desayuno"); 
    console.log("Almuerzo"); 
    console.log("Cena");  
}


class Padre extends Hogar{         //MIEMBROS DE LA FAMILIA
    EquipoDeFutbol;
    JugadorPreferido;
    SerieFavorita;

    GestionarTiempoLibre(){
        console.log("Domingos");
    }
}
let miPadre=new Padre();

class Madre extends Hogar{
    ActorFavorito;
    ComidaPreferida;
    NovelaRecomendada;
    PeliculaFavorita;

    GestionarTiempoLibre(){
        console.log("Sabados y Domingos");
    }
}
let miMadre=new Madre();

class Hija_Mayor extends Hogar{
    BandasDeRock;
    LibroPreferido;
    AutorFavorito;
    CancionesPreferidas;

    laborEnCasa(){
        Aseo();
    }
}
let miHija_Mayor=new Hija_Mayor();

class Hijo_Menor extends Hogar{
    PostreFavorito;
    JuegosPreferidos;
    laborEnCasa(){
        Aseo();
    }
}
let miHijo_Menor=new Hijo_Menor();


class Mascotas {                     //MASCOTAS
    Nombre;
    Edad;
    TipoDeComida;
    GestionarTiempoDeCuidado(){
        console.log("Mañana y Noche");
    }
}

class Perro extends Mascotas{                 
    Raza;
    MedicinaVeterinaria;
    JuguetePreferido;
    GalletaPreferida;
    HorasDeJuego
    Ladrar(){
        console.log("Guau, Guau");
    }
}
let miPerro=new Perro();

class Pericos extends Mascotas{               
    HorasDeVuelo;
    Cantar(){
        console.log("Pio,Pio");
    }
}
let miPericos=new Pericos();